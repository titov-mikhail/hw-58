import React from "react";
import './Modal.css'
import Backdrop from "../Backdrop/Backdrop";

const Modal = props => {
    return (
        <>
            <Backdrop
                show={props.show}
                onClick = {props.close}
            />
            <div
                className="Modal"
                style={
                    {
                        transform: props.show ? 'translateY(0)' : 'translateY(-100vh)',
                        opacity: props.show ? '1' : '0',
                    }
                }
            >
                <div className="ModalHeader">
                    <div className="ModalTitle">{props.title}</div>
                    <div className="ModalClose" onClick={props.close}>X</div>
                </div>
                <div className="ModalContent">
                    {props.content}
                </div>
            </div>
        </>
    );
};

export default Modal;