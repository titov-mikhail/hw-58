import React from 'react';
import './Alert.css'

const Alert = (props) => {
    return (
           props.show ?
                <div className={["Alert", props.type].join(" ")}>
                    <div className="AlertMessage">
                        {props.message}
                    </div>
                    {
                        props.dismiss ?
                        <div className="AlertClose" onClick={()=> props.dismiss()}>X</div>
                        : null
                    }

                </div>
                : null
    );
};

export default Alert;