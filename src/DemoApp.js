import React, {useState} from 'react';
import Button from "./UI/Button/Button";
import Modal from "./UI/Modal/Modal";
import Alert from "./UI/Alert/Alert";



const DemoApp = () => {
    const [showModal, setShowModal] = useState(false);
    const [showAlert, setShowAlert] = useState(false);
    const [alertType, setAlertType] = useState("");
    const [useAlertDismiss, setUseAlertDismiss] = useState();

    const showModalHandler = () => {
        setShowModal(true);
    }

    const closeModal = () => {
        setShowModal(false);
    }

    const  dismissAlert = () => {
        setShowAlert(false);
    }

    const openAlert = (typeOfAlert, useDismiss) => {
        setAlertType(typeOfAlert);
        setUseAlertDismiss(useDismiss);
        setShowAlert(true);
    }

    return (
        <div className="DemoApp">
            <Button
                onClick = { ()=>showModalHandler() }
                type = "Success"
                children = "Открыть модальное окно"
            />

            <Button
                onClick = { ()=>openAlert("primary", true) }
                type = "primary"
                children = "Вызвать primary alert"
            />

            <Button
                onClick = { ()=>openAlert("success", true) }
                type = "Success"
                children = "Вызвать success alert"
            />

            <Button
                onClick = { ()=>openAlert("danger", true) }
                type = "Success"
                children = "Вызвать danger alert"
            />

            <Button
                onClick = { ()=>openAlert("warning", true) }
                type = "Success"
                children = "Вызвать warning alert"
            />

            <Button
                onClick = { ()=>openAlert("warning", false) }
                type = "Success"
                children = "Вызвать alert без кнопки закрыть"
            />


            <Modal
                show = {showModal}
                close = {closeModal}
                title = "Заголовок модального окна"
                content = "Содержимое модального окна"
            />

            <Alert
                dismiss = { useAlertDismiss ? ()=>dismissAlert() : undefined }
                show = {showAlert}
                type = {alertType}
                message = {"This is " + alertType + " type alert"}
            />
        </div>
    );
};

export default DemoApp;